#![recursion_limit = "128"]

extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::{
    parse_macro_input, Attribute, DataEnum, DataStruct, DeriveInput, Generics, Ident, Lit, Meta,
    NestedMeta,
};

trait LitExt {
    fn unwrap_str(&self) -> String;
}

impl LitExt for Lit {
    fn unwrap_str(&self) -> String {
        match self {
            Lit::Str(s) => s.value(),
            _ => panic!("Attribute value must be a string literal"),
        }
    }
}

trait NestedMetaExt {
    fn unwrap_lit(&self) -> String;
    fn value(&self) -> Lit;
}

impl NestedMetaExt for NestedMeta {
    fn unwrap_lit(&self) -> String {
        match self {
            NestedMeta::Lit(Lit::Str(s)) => s.value(),
            _ => panic!("Attribute value must be a string literal"),
        }
    }

    fn value(&self) -> Lit {
        match self {
            NestedMeta::Lit(lit) => lit.clone(),
            _ => panic!("Expected literal value, not another meta"),
        }
    }
}

#[derive(Clone, Copy)]
enum RenameTo {
    Lowercase,
    Uppercase,
    SnakeCase,
    KebabCase,
    CamelCase,
    MixedCase,
    ShoutySnakeCase,
}

impl RenameTo {
    fn from_str(s: &str) -> Self {
        match s {
            "snake_case" => RenameTo::SnakeCase,
            "kebab-case" => RenameTo::KebabCase,
            "camelCase" => RenameTo::MixedCase,
            "PascalCase" => RenameTo::CamelCase,
            "SCREAMING_SNAKE_CASE" => RenameTo::ShoutySnakeCase,
            "UPPERCASE" => RenameTo::Uppercase,
            "lowercase" => RenameTo::Lowercase,
            _ => panic!("Unknown rename format"),
        }
    }
}

#[derive(Default)]
struct OuterAttributeOptions {
    rename: Option<String>,
    rename_all: Option<RenameTo>,
    attributes: Vec<(String, String)>,
    namespace: Option<String>,
}

impl OuterAttributeOptions {
    fn from_attrs(attrs: impl Iterator<Item = NestedMeta>) -> Self {
        let mut opts = Self::default();

        for attr in attrs {
            let meta = if let NestedMeta::Meta(meta) = attr {
                meta
            } else {
                panic!("Unknown derive macro option");
            };

            match meta {
                Meta::List(list) => {
                    let s = list.path.get_ident().unwrap().to_string();

                    match s.as_str() {
                        "attr" => {
                            if list.nested.len() != 2 {
                                panic!("Invalid number of arguments to `attr`");
                            }
                            let name = list.nested.first().unwrap().value().unwrap_str();
                            let value = list.nested.last().unwrap().value().unwrap_str();

                            opts.attributes.push((name, value));
                        }
                        s => panic!("Unknown derive macro option `{}`", s),
                    }
                }
                Meta::NameValue(pair) => {
                    let s = pair.path.get_ident().unwrap().to_string();

                    match s.as_str() {
                        "rename" => opts.rename = Some(pair.lit.unwrap_str()),
                        "rename_all" => {
                            opts.rename_all = Some(RenameTo::from_str(&pair.lit.unwrap_str()))
                        }
                        "ns" => opts.namespace = Some(pair.lit.unwrap_str()),
                        s => panic!("Unknown derive macro option `{}`", s),
                    }
                }
                _ => panic!("Unknown derive macro option type"),
            }
        }

        opts
    }
}

#[derive(Default)]
struct InnerAttributeOptions {
    is_collection: bool,
    is_attribute: bool,
    is_bare_value: bool,
    namespace: Option<String>,
    rename: Option<String>,
    skip_if: Option<syn::Path>,
    wrap_in: Option<String>,
}

impl InnerAttributeOptions {
    fn from_attrs(attrs: impl Iterator<Item = NestedMeta>) -> Self {
        let mut opts = Self::default();

        for attr in attrs {
            let meta = if let NestedMeta::Meta(meta) = attr {
                meta
            } else {
                panic!("Unknown derive macro option");
            };

            match meta {
                Meta::Path(ident) => {
                    let s = ident.get_ident().unwrap().to_string();

                    match s.as_str() {
                        "attribute" => opts.is_attribute = true,
                        "bare_value" => opts.is_bare_value = true,
                        "collection" => opts.is_collection = true,
                        s => panic!("Unknown derive macro option `{}`", s),
                    }
                }
                Meta::NameValue(pair) => {
                    let s = pair.path.get_ident().unwrap().to_string();

                    match s.as_str() {
                        "rename" => opts.rename = Some(pair.lit.unwrap_str()),
                        "wrap_in" => opts.wrap_in = Some(pair.lit.unwrap_str()),
                        "skip_if" => {
                            opts.skip_if = Some(
                                syn::parse_str(&pair.lit.unwrap_str())
                                    .expect("Valid function path"),
                            );
                        }
                        "ns" => opts.namespace = Some(pair.lit.unwrap_str()),
                        s => panic!("Unknown derive macro option `{}`", s),
                    }
                }
                _ => panic!("Unknown derive macro option type"),
            }
        }

        if opts.is_attribute && opts.is_bare_value {
            panic!("The `attribute` and `bare_value` options are mutually exclusive");
        } else if opts.is_attribute && opts.wrap_in.is_some() {
            panic!("The `attribute` and `wrap_in` options are mutually exclusive");
        } else if opts.is_bare_value && opts.rename.is_some() {
            panic!("The `rename` attribute has no effect when `bare_value` is specified");
        } else if opts.is_attribute && opts.is_collection {
            panic!("The `attribute` and `collection` options are mutually exclusive");
        }

        opts
    }
}

#[proc_macro_derive(ToXmlElement, attributes(xml))]
pub fn derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let item_attrs = OuterAttributeOptions::from_attrs(xml_attrs(&input.attrs));
    match input.data {
        syn::Data::Struct(s) => derive_struct(item_attrs, input.ident, input.generics, s),
        syn::Data::Enum(e) => derive_enum(item_attrs, input.ident, input.generics, e),
        _ => panic!("Unions are not supported"),
    }
}

fn derive_struct(
    struct_options: OuterAttributeOptions,
    ident: Ident,
    mut generics: Generics,
    data: DataStruct,
) -> TokenStream {
    let named = if let syn::Fields::Named(named) = data.fields {
        named
    } else {
        panic!("Unnamed fields and unit structs are not supported")
    };

    add_trait_bound(&mut generics);

    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let struct_name = if let Some(rename) = &struct_options.rename {
        let mut rename = rename.clone();

        if let Some(ns) = &struct_options.namespace {
            rename = format!("{}:{}", ns, rename);
        }

        quote! { #rename }
    } else if let Some(case) = struct_options.rename_all {
        let mut formatted_name = rename_field(&ident.to_string(), case);
        if let Some(ns) = &struct_options.namespace {
            formatted_name = format!("{}:{}", ns, formatted_name);
        }
        quote! { #formatted_name }
    } else {
        let name = if let Some(ns) = &struct_options.namespace {
            format!("{}:{}", ns, ident)
        } else {
            ident.to_string()
        };
        quote! { #name }
    };

    let attrs = struct_options
        .attributes
        .iter()
        .fold(quote!(), |mut ts, (name, value)| {
            ts.extend(quote! { attributes.insert(#name.to_string(), #value.to_string()); });
            ts
        });

    let mut field_additions = quote!();

    for field in &named.named {
        let field_options = InnerAttributeOptions::from_attrs(xml_attrs(&field.attrs));
        let field_ident = field.ident.as_ref().unwrap();

        let mut rename = if let Some(rename) = &field_options.rename {
            rename.clone()
        } else if let Some(case) = struct_options.rename_all {
            rename_field(&field_ident.to_string(), case)
        } else {
            field_ident.to_string()
        };

        if let Some(ns) = &field_options.namespace {
            rename = format!("{}:{}", ns, rename);
        }

        if let Some(path) = field_options.skip_if {
            field_additions.extend(quote!(if !#path(&self.#field_ident)));
        }

        if field_options.is_attribute {
            field_additions.extend(quote! {
                {attributes.insert(#rename.to_string(), self.#field_ident.to_string());}
            });
        } else if field_options.is_bare_value && field_options.is_collection {
            field_additions.extend(if let Some(wrapper) = &field_options.wrap_in {
                quote! { {children.push(XmlNode::Element(XmlElement::new(#wrapper, Attributes::default(), self.#field_ident.iter().map(ToXmlNode::to_xml_node).collect())));} }
            } else {
                quote! { {children.extend(self.#field_ident.iter().map(ToXmlNode::to_xml_node).collect::<Vec<_>>());} }
            });
        } else if field_options.is_bare_value {
            field_additions.extend(if let Some(wrapper) = &field_options.wrap_in {
                quote! { {children.push(XmlNode::Element(XmlElement::new(#wrapper, Attributes::default(), vec![self.#field_ident.to_xml_node()])));} }
            } else {
                quote! { {children.push(self.#field_ident.to_xml_node());} }
            });
        } else if field_options.is_collection {
            field_additions.extend(if let Some(wrapper) = &field_options.wrap_in {
                quote! { {children.push(XmlNode::Element(XmlElement::new(#wrapper, Attributes::default(), vec![XmlNode::Element(XmlElement::new(#rename, Attributes::default(), self.#field_ident.iter().map(ToXmlNode::to_xml_node).collect()))])));} }
            } else {
                quote! { {children.push(XmlNode::Element(XmlElement::new(#rename, Attributes::default(), self.#field_ident.iter().map(ToXmlNode::to_xml_node).collect())));} }
            });
        } else {
            field_additions.extend(if let Some(wrapper) = &field_options.wrap_in {
                quote! { {children.push(XmlNode::Element(XmlElement::new(#wrapper, Attributes::default(), vec![XmlNode::Element(XmlElement::new(#rename, Attributes::default(), vec![self.#field_ident.to_xml_node()]))])));} }
            } else {
                quote! { {children.push(XmlNode::Element(XmlElement::new(#rename, Attributes::default(), vec![self.#field_ident.to_xml_node()])));} }
            });
        }
    }

    TokenStream::from(quote! {
        impl #impl_generics ::xml_dom::ToXmlElement for #ident #ty_generics #where_clause {
            fn to_xml_element(&self) -> ::xml_dom::XmlElement {
                use ::xml_dom::{XmlElement, XmlNode, ToXmlNode, Attributes};

                let mut attributes = Attributes::new();
                let mut children = vec![];
                let name = #struct_name;

                #attrs

                #field_additions

                XmlElement::new(#struct_name, attributes, children)
            }
        }
    })
}

fn derive_enum(
    _struct_options: OuterAttributeOptions,
    _ident: Ident,
    _generics: Generics,
    _data: DataEnum,
) -> TokenStream {
    unimplemented!()
}

fn rename_field(name_string: &str, rename_to: RenameTo) -> String {
    use heck::*;

    match rename_to {
        RenameTo::CamelCase => name_string.to_camel_case(),
        RenameTo::KebabCase => name_string.to_kebab_case(),
        RenameTo::MixedCase => name_string.to_mixed_case(),
        RenameTo::ShoutySnakeCase => name_string.to_shouty_snake_case(),
        RenameTo::SnakeCase => name_string.to_snake_case(),
        RenameTo::Uppercase => name_string.to_uppercase(),
        RenameTo::Lowercase => name_string.to_lowercase(),
    }
}

fn add_trait_bound(generics: &mut Generics) {
    for param in &mut generics.params {
        if let syn::GenericParam::Type(type_param) = param {
            type_param
                .bounds
                .push(syn::parse_quote! { ::xml_dom::ToXmlNode })
        }
    }
}

fn xml_attrs<'a>(attributes: &'a [Attribute]) -> impl Iterator<Item = NestedMeta> + 'a {
    attributes
        .iter()
        .map(|a| a.parse_meta().expect("Failed to parse attribute"))
        .filter_map(|m| {
            if let Meta::List(list) = m {
                if list.path.get_ident().unwrap() == "xml" {
                    return Some(list);
                }
            }

            None
        })
        .map(|list| list.nested)
        .flat_map(|n| n.into_iter())
}
