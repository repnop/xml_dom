# xml_dom

`xml_dom` provides an XML parser that produces an XML DOM with the eventual goal
of allowing for DTD validation upon user request.

### Example
```rust
use std::str::FromStr;

fn main() {
    let simple = r#"
<root>
    <foo bar="baz" qux='quack'>aaaa bbbb cccc</foo>
    <aaa><?processing instruction?></aaa>
    hello world
    <![CDATA[dis is some cdata text]]>
    <!-- comment -->
</root>"#;

    let root = xml_dom::XmlDocument::from_str(simple).unwrap();

    println!("{:#?}", root);
}
```

### Derive Macros

`xml_dom` has the ability to automatically serialize your structs (and soon™ enums) to XML elements (and by extension, text!).

#### Example
```rust
use xml_dom::ToXmlElement;

#[derive(ToXmlElement)]
#[xml(rename = "my-struct")]
struct MyStruct {
    hey: i32,
    some: Option<u32>,
    fields: String,
}

fn main() {
    let my_struct = MyStruct {
        hey: 1,
        some: Some(5),
        fields: "Yay!".into()
    };

    println!("{}", my_struct.to_xml_element().pretty_print(2));
}
```
This produces:
```xml
<my-struct>
  <hey>1</hey>
  <some>5</some>
  <fields>Yay!</fields>
</my-struct>
```

