use xml_dom::ToXmlElement;

#[derive(ToXmlElement)]
#[xml(attr("xmlns:soapenv", "http://www.w3.org/2003/05/soap-envelope/"))]
#[xml(ns = "soapenv")]
struct Envelope {
    header: Header,
    body: Body,
}

#[derive(ToXmlElement)]
#[xml(ns = "soapenv")]
struct Header {
    #[xml(rename = "favoriteColor")]
    favorite_color: String,
}

#[derive(ToXmlElement)]
#[xml(ns = "soapenv")]
struct Body {
    about_me: AboutMe,
}

#[derive(ToXmlElement)]
struct AboutMe {
    name: String,
    favorite_language: String,
    #[xml(attribute)]
    gitlab: String,
}

fn main() {
    println!(
        "{}",
        Envelope {
            header: Header {
                favorite_color: "Green".into(),
            },
            body: Body {
                about_me: AboutMe {
                    name: "repnop".into(),
                    favorite_language: "Rust".into(),
                    gitlab: "https://gitlab.com/repnop/".into(),
                }
            }
        }
        .to_xml_element()
        .pretty_print(2)
    )
}
