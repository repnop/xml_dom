use std::str::FromStr;
use xml_dom::XmlDocument;

fn main() {
    let xml = XmlDocument::from_str(r#"<?xml version="1.0" encoding="UTF-8" standalone="yes" ?><root><bar><baz>5</baz><qux/></bar></root>"#).unwrap();
    println!("{}", xml.pretty_print(2));

    let xml = XmlDocument::from_str("<root><bar><baz>5</baz><qux/></bar></root>").unwrap();
    println!("{}", xml.pretty_print(2));

    let xml = XmlDocument::from_str("<root>abc</root>").unwrap();
    println!("{}", xml.pretty_print(2));

    let xml = XmlDocument::from_str("<root><a>abc<b></b>def</a></root>").unwrap();
    println!("{}", xml.pretty_print(2));
}
