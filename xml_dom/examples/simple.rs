use std::str::FromStr;
use xml_dom::XmlDocument;

fn main() {
    let xml = r#"
        <root>
            <child1 attr1="foo">some text</child1>
            <child2 this-is="a short element" />
            <![CDATA[hey this is some CDATA text!]]>
            <!-- we got comments too -->
        </root>
    "#;

    println!("{}", XmlDocument::from_str(xml).unwrap().root().pretty_print(2));
}
