use xml_dom::ToXmlElement;

fn snake_case() {
    #[allow(non_snake_case)]
    #[derive(ToXmlElement)]
    #[xml(rename_all = "snake_case")]
    struct Letter {
        SendTo: String,
        From: String,
        MessageToSend: String,
    }

    let letter = Letter {
        SendTo: "Jove".into(),
        From: "Tani".into(),
        MessageToSend: "Don't forget me this weekend!".into(),
    };

    println!("{}\n", letter.to_xml_element());
}

fn camel_case() {
    #[derive(ToXmlElement)]
    #[xml(rename_all = "camelCase")]
    struct Letter {
        send_to: String,
        from: String,
        message_to_send: String,
    }

    let letter = Letter {
        send_to: "Jove".into(),
        from: "Tani".into(),
        message_to_send: "Don't forget me this weekend!".into(),
    };

    println!("{}\n", letter.to_xml_element());
}

fn pascal_case() {
    #[derive(ToXmlElement)]
    #[xml(rename_all = "PascalCase")]
    struct Letter {
        send_to: String,
        from: String,
        message_to_send: String,
    }

    let letter = Letter {
        send_to: "Jove".into(),
        from: "Tani".into(),
        message_to_send: "Don't forget me this weekend!".into(),
    };

    println!("{}\n", letter.to_xml_element());
}

fn shouting_snake_case() {
    #[derive(ToXmlElement)]
    #[xml(rename_all = "SCREAMING_SNAKE_CASE")]
    struct Letter {
        send_to: String,
        from: String,
        message_to_send: String,
    }

    let letter = Letter {
        send_to: "Jove".into(),
        from: "Tani".into(),
        message_to_send: "Don't forget me this weekend!".into(),
    };

    println!("{}\n", letter.to_xml_element());
}

fn uppercase() {
    #[derive(ToXmlElement)]
    #[xml(rename_all = "UPPERCASE")]
    struct Letter {
        sendto: String,
        from: String,
        messagetosend: String,
    }

    let letter = Letter {
        sendto: "Jove".into(),
        from: "Tani".into(),
        messagetosend: "Don't forget me this weekend!".into(),
    };

    println!("{}\n", letter.to_xml_element());
}

fn lowercase() {
    #[derive(ToXmlElement)]
    #[allow(non_snake_case)]
    #[xml(rename_all = "lowercase")]
    struct Letter {
        SendTo: String,
        From: String,
        MessageToSend: String,
    }

    let letter = Letter {
        SendTo: "Jove".into(),
        From: "Tani".into(),
        MessageToSend: "Don't forget me this weekend!".into(),
    };

    println!("{}\n", letter.to_xml_element());
}

fn main() {
    snake_case();
    camel_case();
    pascal_case();
    shouting_snake_case();
    uppercase();
    lowercase();
}
