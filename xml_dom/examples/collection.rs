use xml_dom::ToXmlElement;

#[derive(ToXmlElement)]
#[xml(rename = "todoList")]
struct ToDoList {
    #[xml(collection)]
    today: Vec<ToDo>,
}

#[derive(ToXmlElement)]
#[xml(rename = "todo")]
struct ToDo {
    #[xml(bare_value)]
    note: &'static str,
}

impl ToDo {
    fn new(note: &'static str) -> Self {
        Self { note }
    }
}

fn main() {
    let list = ToDoList {
        today: vec![ToDo::new("Get groceries"), ToDo::new("Laundry")],
    };

    println!("{}", list.to_xml_element());
}