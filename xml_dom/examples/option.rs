use xml_dom::ToXmlElement;

#[derive(ToXmlElement)]
#[xml(rename_all = "camelCase")]
struct Letter {
    send_to: String,
    from: String,
    message_to_send: String,
    #[xml(skip_if = "Option::is_none")]
    signature: Option<String>,
}

#[derive(ToXmlElement)]
#[xml(rename_all = "camelCase")]
struct Letter2 {
    send_to: String,
    from: String,
    message_to_send: String,
    // By default, `None` values for `Option<T>` will serialize into a blank
    // text node.
    signature: Option<String>,
}

fn main() {
    // No `skip_if = "Option::is_none"`
    let letter = Letter {
        send_to: "Jove".into(),
        from: "Tani".into(),
        message_to_send: "Don't forget me this weekend!".into(),
        signature: Some("-- Tani".into()),
    };

    println!("{}\n", letter.to_xml_element());

    let letter = Letter {
        send_to: "Jove".into(),
        from: "Tani".into(),
        message_to_send: "Don't forget me this weekend!".into(),
        signature: None,
    };

    println!("{}\n\n\n", letter.to_xml_element());

    // Default behavior
    let letter = Letter2 {
        send_to: "Jove".into(),
        from: "Tani".into(),
        message_to_send: "Don't forget me this weekend!".into(),
        signature: Some("-- Tani".into()),
    };

    println!("{}\n", letter.to_xml_element());

    let letter = Letter2 {
        send_to: "Jove".into(),
        from: "Tani".into(),
        message_to_send: "Don't forget me this weekend!".into(),
        signature: None,
    };

    println!("{}\n", letter.to_xml_element());
}
