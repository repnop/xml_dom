use crate::{XmlDocument, XmlElement, XmlNode};
use std::fmt::{Display, Formatter, Result};

pub enum DocOrElement<'a> {
    Doc(&'a XmlDocument),
    Element(&'a XmlElement),
}

/// Wrapper struct that pretty prints when displayed.
pub struct XmlPrettyPrinter<'a> {
    pub inner: DocOrElement<'a>,
    pub indent_length: u32,
}

impl<'a> Display for XmlPrettyPrinter<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let root = match self.inner {
            DocOrElement::Doc(document) => {
                if let Some(decl) = document.xml_declaration() {
                    writeln!(f, "{}", decl)?;
                }

                for pi in document
                    .processing_instructions()
                    .iter()
                    .filter(|pi| pi.position == crate::parser::Position::Before)
                {
                    writeln!(f, "{}", pi)?;
                }

                document.root()
            }
            DocOrElement::Element(elem) => elem,
        };

        pretty_print_inner_element(root, f, self.indent_length, 0)?;

        if let DocOrElement::Doc(document) = self.inner {
            for pi in document
                .processing_instructions()
                .iter()
                .filter(|pi| pi.position == crate::parser::Position::After)
            {
                writeln!(f, "{}", pi)?;
            }
        }

        Ok(())
    }
}

fn pretty_print_inner_node(
    node: &XmlNode,
    f: &mut Formatter,
    indent_length: u32,
    indent_level: u32,
    short_print_text: bool,
) -> Result {
    match node {
        XmlNode::Element(e) => pretty_print_inner_element(e, f, indent_length, indent_level),
        XmlNode::Text(txt) => {
            if short_print_text {
                write!(f, "{}", txt)
            } else if !txt.trim().is_empty() {
                write_indents(f, indent_length, indent_level)?;
                writeln!(f, "{}", txt)
            } else {
                Ok(())
            }
        }
        XmlNode::ProcessingInstruction(pi) => writeln!(f, "{}", pi),
    }
}

fn pretty_print_inner_element(
    element: &XmlElement,
    f: &mut Formatter,
    indent_length: u32,
    indent_level: u32,
) -> Result {
    write_indents(f, indent_length, indent_level)?;
    write!(f, "<{}", element.name())?;

    for (name, value) in element.attributes() {
        write!(f, " {}=\"{}\"", name, value)?;
    }

    if element.children().is_empty() {
        writeln!(f, " />")
    } else {
        let short_print =
            element.children().len() == 1 && element.children().iter().any(XmlNode::is_text);

        if !short_print {
            writeln!(f, ">")?;

            for child in element.children() {
                pretty_print_inner_node(child, f, indent_length, indent_level + 1, false)?;
            }

            write_indents(f, indent_length, indent_level)?;
            writeln!(f, "</{}>", element.name())
        } else {
            write!(f, ">")?;
            for child in element.children() {
                pretty_print_inner_node(child, f, indent_length, indent_level + 1, true)?;
            }
            writeln!(f, "</{}>", element.name())
        }
    }
}

fn write_indents(f: &mut Formatter, indent_length: u32, indent_level: u32) -> Result {
    for _ in 0..(indent_length * indent_level) {
        write!(f, " ")?;
    }

    Ok(())
}
