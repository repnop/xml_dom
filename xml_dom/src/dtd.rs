use super::XmlProcessingInstruction;

#[derive(Debug, Clone, PartialEq)]
#[doc(hidden)]
pub struct XmlDocTypeDeclaration {
    name: String,
    external_id: Option<XmlExternalId>,
    declarations: Vec<XmlMarkupDeclaration>,
}

#[derive(Debug, Clone, PartialEq)]
#[doc(hidden)]
pub enum XmlMarkupDeclaration {
    PercentEncodedReference(String),
    Element(String, XmlContentSpec),
    Attribute,
    Entity,
    Notation,
    ProcessingInstruction(XmlProcessingInstruction),
}

#[derive(Debug, Clone, PartialEq)]
#[doc(hidden)]
pub enum XmlContentSpec {
    Empty,
    Any,
    /// `#PCDATA` is implied
    Mixed(Vec<String>),
    Children(XmlContentChildren),
}

#[derive(Debug, Clone, PartialEq)]
#[doc(hidden)]
pub enum XmlContentChildren {
    Name(String),
    ZeroOrOne(Vec<XmlContentChildren>),
    ZeroOrMore(Vec<XmlContentChildren>),
    OneOrMore(Vec<XmlContentChildren>),
    Choice(Vec<XmlContentChildren>),
    Sequence(Vec<XmlContentChildren>),
}

#[derive(Debug, Clone, PartialEq)]
#[doc(hidden)]
pub enum XmlExternalId {
    System(String),
    Public(String, String),
}
