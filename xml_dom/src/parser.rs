mod char_consts;

use crate::*;
use char_consts::*;
use std::{
    collections::VecDeque,
    io::{self, Read},
};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Position {
    After,
    Before,
    Inside,
}

/// Enum representing possible errors during parsing of the XML document.
#[derive(Debug)]
pub enum XmlParseErrorKind {
    /// An attribute of the same name was already defined in the element.
    AttributeAlreadyExists(String),
    /// Invalid character data.
    InvalidCharData(char),
    /// Invalid character reference.
    InvalidCharRef(String),
    /// Invalid element contents.
    InvalidElementContents(String),
    /// An invalid processing instruction target. This only occurs when some
    /// casing of "xml" is present in a processing instruction.
    InvalidProcessingInstructionTarget,
    /// An invalid PubId literal character.
    InvalidPubIdLiteral(char),
    /// An invalid tag name.
    InvalidTagName(String),
    /// XML contained malformed UTF-8.
    InvalidUtf8,
    /// An invalid XML encoding string.
    InvalidXmlEncoding,
    /// An invalid standalone property string.
    InvalidXmlStandaloneValue,
    /// An invalid XML version string.
    InvalidXmlVersion,
    /// An error occurred during reading.
    IoError(io::Error),
    /// An invalid duplicated XML version declaration.
    RepeatedXmlDeclaration,
    /// An unexpected CDATA declaration.
    UnexpectedCdata,
    /// Unexpected character encountered during parsing.
    UnexpectedChar(char),
    /// An invalid closing tag name. ex. `<tag1></tag2>`
    UnexpectedClosingTagName(String),
    /// An unexpected element was encountered.
    UnexpectedElement,
    /// EOF was hit during parsing when it wasn't expected.
    UnexpectedEndOfText,
}

impl From<io::Error> for XmlParseErrorKind {
    fn from(e: io::Error) -> Self {
        if e.kind() == io::ErrorKind::UnexpectedEof {
            XmlParseErrorKind::UnexpectedEndOfText
        } else {
            XmlParseErrorKind::IoError(e)
        }
    }
}

impl From<std::str::Utf8Error> for XmlParseErrorKind {
    fn from(_: std::str::Utf8Error) -> Self {
        XmlParseErrorKind::InvalidUtf8
    }
}

#[derive(Debug)]
/// XML parse error with line and column information.
pub struct XmlParseError {
    /// The kind of parse error.
    pub kind: XmlParseErrorKind,
    /// The line number in the source at which the error occurred.
    pub line: usize,
    /// The column number in the line at which the error occurred.
    pub column: usize,
}

impl XmlParseError {
    fn new(kind: XmlParseErrorKind, line: usize, column: usize) -> Self {
        Self { kind, line, column }
    }
}

impl std::error::Error for XmlParseError {}

impl fmt::Display for XmlParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "line {}, col {}: {:?}",
            self.line, self.column, self.kind
        )
    }
}

#[derive(Debug, Clone, Copy)]
/// Options to pass to the parser to modify its behavior.
pub struct ParserOptions {
    /// Whether to preserve whitespace between elements.
    ///
    /// Default value: `true`
    pub whitespace_between_elements: bool,
    /// Whether to keep namespace prefixes in tag names.
    ///
    /// ex. `<ns1:foo />` becomes `<foo />` in the DOM if set to `false`.
    ///
    /// Default value: `true`
    pub tag_name_namespaces: bool,
}

impl Default for ParserOptions {
    fn default() -> Self {
        Self {
            whitespace_between_elements: true,
            tag_name_namespaces: true,
        }
    }
}

impl ParserOptions {
    /// Create a new, default instance of the options.
    pub fn new() -> Self {
        Self::default()
    }

    /// Enable/disable trimming whitespace between elements
    pub fn whitespace_between_elements(mut self, b: bool) -> Self {
        self.whitespace_between_elements = b;
        self
    }

    /// Enable/disable trimming namespaces in tag names
    pub fn tag_name_namespaces(mut self, b: bool) -> Self {
        self.tag_name_namespaces = b;
        self
    }
}

pub struct Parser<R: Read> {
    inner: R,
    line: usize,
    col: usize,
    buffer: String,
    peek: VecDeque<char>,
    options: ParserOptions,
}

impl<R: Read> Parser<R> {
    pub fn new(inner: R) -> Self {
        Self {
            inner,
            line: 1,
            col: 1,
            buffer: String::with_capacity(256),
            peek: VecDeque::new(),
            options: ParserOptions::default(),
        }
    }

    pub fn with_options(inner: R, options: ParserOptions) -> Self {
        Self {
            inner,
            line: 1,
            col: 1,
            buffer: String::with_capacity(256),
            peek: VecDeque::new(),
            options,
        }
    }

    pub fn parse(&mut self) -> Result<XmlDocument, XmlParseError> {
        self.parse_inner().map_err(|kind| XmlParseError {
            kind,
            line: self.line,
            column: self.col,
        })
    }

    pub fn parse_inner(&mut self) -> Result<XmlDocument, XmlParseErrorKind> {
        self.eat_whitespace()?;
        let mut doc = self.parse_prolog()?;
        doc.root = XmlNode::Element(self.parse_element()?);

        let eof_ok_or_err = |r| -> Result<&'static str, XmlParseErrorKind> {
            match r {
                Ok(_) => Ok(""),
                Err(e) => {
                    if let XmlParseErrorKind::UnexpectedEndOfText = e {
                        Ok("eof")
                    } else {
                        Err(e)
                    }
                }
            }
        };

        loop {
            if eof_ok_or_err(self.eat_whitespace())? == "eof" {
                break;
            }

            if self.peek_str("<!--")? {
                self.eat_comment()?;
            } else if self.peek_str("<?")? {
                doc.processing_instructions
                    .push(self.parse_processing_instruction(Position::After)?);
            } else if self.peek()? == '<' {
                return Err(XmlParseErrorKind::UnexpectedElement);
            } else {
                return Err(XmlParseErrorKind::UnexpectedChar(self.next()?));
            }
        }

        Ok(doc)
    }

    pub fn parse_prolog(&mut self) -> Result<XmlDocument, XmlParseErrorKind> {
        let mut xml_declaration = None;
        let doctype_declarations = Vec::new();
        let mut processing_instructions = Vec::new();

        self.eat_whitespace()?;

        if self.peek_str("<?xml")? {
            xml_declaration = Some(self.parse_xml_declaration()?);
        }

        loop {
            self.eat_whitespace()?;

            if self.peek_str("<!--")? {
                self.eat_comment()?;
            } else if self.peek_str("<?")? {
                processing_instructions.push(self.parse_processing_instruction(Position::Before)?);
            } else if self.peek_str("<![CDATA[")? {
                return Err(XmlParseErrorKind::UnexpectedCdata);
            } else if self.peek_str("<!")? {
                // TODO: this is where DTDs would be parsed.
                let mut stack = vec!['<'];
                self.next().unwrap();

                let mut in_quotes = false;

                while !stack.is_empty() {
                    let next = self.next()?;

                    if next == '"' {
                        in_quotes = !in_quotes;
                        continue;
                    }

                    if !in_quotes {
                        if next == '<' {
                            stack.push('<');
                        } else if next == '>' {
                            stack.pop();
                        }
                    }
                }
            } else {
                break;
            }
        }

        Ok(XmlDocument {
            xml_declaration,
            doctype_declarations,
            processing_instructions,
            root: XmlNode::Element(XmlElement::default()),
        })
    }

    fn parse_xml_declaration(&mut self) -> Result<XmlDeclaration, XmlParseErrorKind> {
        self.eat_str("<?xml").unwrap();

        self.eat_whitespace()?;

        self.eat_str("version")?;

        self.eat_eq()?;

        let quote_type = match self.peek()? {
            c if c == '\'' || c == '"' => c,
            c => return Err(XmlParseErrorKind::UnexpectedChar(c)),
        };

        self.eat_char(quote_type).unwrap();

        self.collect_while(|c| match c {
            'a'..='z' | 'A'..='Z' | '0'..='9' | '_' | '.' | ':' | '-' => true,
            _ => false,
        })?;

        if self.buffer.is_empty() {
            return Err(XmlParseErrorKind::InvalidXmlVersion);
        }

        self.eat_char(quote_type)?;

        let version = self.buffer.clone();
        let mut encoding = None;
        let mut standalone = None;

        self.eat_whitespace()?;

        if self.peek_str("encoding")? {
            self.eat_str("encoding").unwrap();
            self.eat_eq()?;

            let quote_type = match self.peek()? {
                c if c == '\'' || c == '"' => c,
                c => return Err(XmlParseErrorKind::UnexpectedChar(c)),
            };

            self.eat_char(quote_type).unwrap();

            match self.peek()? {
                'A'..='Z' | 'a'..='z' => {}
                _ => return Err(XmlParseErrorKind::InvalidXmlEncoding),
            }

            self.collect_while(|c| match c {
                'a'..='z' | 'A'..='Z' | '0'..='9' | '_' | '.' | ':' | '-' => true,
                _ => false,
            })?;

            self.eat_char(quote_type)?;

            if self.buffer.is_empty() {
                return Err(XmlParseErrorKind::InvalidXmlEncoding);
            }

            encoding = Some(self.buffer.clone());

            self.eat_whitespace()?;
        }

        if self.peek_str("standalone")? {
            self.eat_str("standalone").unwrap();
            self.eat_eq()?;

            let quote_type = match self.peek()? {
                c if c == '\'' || c == '"' => c,
                c => return Err(XmlParseErrorKind::UnexpectedChar(c)),
            };

            self.eat_char(quote_type).unwrap();

            match self.peek()? {
                'A'..='Z' | 'a'..='z' => {}
                _ => return Err(XmlParseErrorKind::InvalidXmlEncoding),
            }

            self.collect_while(|c| match c {
                'a'..='z' | 'A'..='Z' | '0'..='9' | '_' | '.' | ':' | '-' => true,
                _ => false,
            })?;

            self.eat_char(quote_type)?;

            if self.buffer.is_empty() {
                return Err(XmlParseErrorKind::InvalidXmlEncoding);
            }

            if self.buffer.eq_ignore_ascii_case("yes") {
                standalone = Some(true);
            } else if self.buffer.eq_ignore_ascii_case("no") {
                standalone = Some(false);
            } else {
                return Err(XmlParseErrorKind::InvalidXmlStandaloneValue);
            }

            self.eat_whitespace()?;
        }

        self.eat_str("?>")?;

        Ok(XmlDeclaration {
            version,
            encoding,
            standalone,
        })
    }

    fn parse_element(&mut self) -> Result<XmlElement, XmlParseErrorKind> {
        self.eat_whitespace()?;
        self.eat_char('<')?;
        let mut name = self.parse_name()?;
        let attributes = self.parse_attributes()?;

        // Any whitespace after the name is eaten by `self.parse_attributes()?`

        if self.peek_str("/>")? {
            self.eat_str("/>").unwrap();

            if !self.options.tag_name_namespaces {
                if let Some(colon_idx) = name.find(':') {
                    let _ = name.drain(..=colon_idx);
                }
            }

            Ok(XmlElement {
                attributes,
                children: vec![],
                name,
            })
        } else {
            self.eat_char('>')?;

            let children = self.parse_content()?;

            self.eat_end_tag(&name)?;

            if !self.options.tag_name_namespaces {
                if let Some(colon_idx) = name.find(':') {
                    let _ = name.drain(..=colon_idx);
                }
            }

            Ok(XmlElement {
                attributes,
                children,
                name,
            })
        }
    }

    fn parse_content(&mut self) -> Result<Vec<XmlNode>, XmlParseErrorKind> {
        let mut contents = Vec::new();

        loop {
            if self.peek_str("</")? {
                break;
            } else if self.peek_str("<!--")? {
                self.eat_comment()?;
            } else if self.peek_str("<![CDATA[")? {
                //
                contents.push(XmlNode::Text(self.parse_cdata()?));
            } else if self.peek_str("<?")? {
                contents.push(XmlNode::ProcessingInstruction(
                    self.parse_processing_instruction(Position::Inside)?,
                ));
            } else if self.peek()? == '<' {
                contents.push(XmlNode::Element(self.parse_element()?));
            } else {
                let mut str_contents = String::new();

                while self.peek()? != '<' {
                    let peek = self.peek()?;

                    if self.peek_str("&#")? {
                        str_contents.push(self.parse_char_ref()?);
                    } else if peek == '&' {
                        let entity_ref = self.parse_entity_reference()?;
                        // TODO resolve ref
                        str_contents.push_str(&entity_ref);
                    } else if peek == '\r' {
                        // Normalize line endings to a single `\n`
                        self.next().unwrap();

                        if self.peek()? == '\n' {
                            self.next().unwrap();
                        }

                        str_contents.push('\n');
                    } else {
                        str_contents.push(self.next()?);
                    }
                }

                if str_contents.contains("]]>") {
                    return Err(XmlParseErrorKind::InvalidElementContents("]]>".into()));
                }

                let trimmed = str_contents.trim();

                if !trimmed.is_empty() || self.options.whitespace_between_elements {
                    contents.push(XmlNode::Text(str_contents));
                }
            }
        }

        Ok(contents)
    }

    fn parse_attributes(&mut self) -> Result<Attributes, XmlParseErrorKind> {
        let mut attributes = Attributes::new();

        loop {
            self.eat_whitespace()?;
            let peek = self.peek()?;

            if peek == '/' || peek == '>' {
                break;
            }

            let (name, value) = self.parse_attribute()?;

            if attributes.get(&name).is_some() {
                return Err(XmlParseErrorKind::AttributeAlreadyExists(name));
            }

            attributes.insert(name, value);
        }

        Ok(attributes)
    }

    fn parse_attribute(&mut self) -> Result<(String, String), XmlParseErrorKind> {
        let name = self.parse_name()?;
        self.eat_eq()?;
        let value = self.parse_attr_value()?;

        Ok((name, value))
    }

    fn parse_attr_value(&mut self) -> Result<String, XmlParseErrorKind> {
        let quote_type = match self.peek()? {
            c if c == '\'' || c == '"' => c,
            c => return Err(XmlParseErrorKind::UnexpectedChar(c)),
        };

        self.eat_char(quote_type)?;

        let mut s = String::new();

        loop {
            let peek = self.peek()?;

            if peek == quote_type {
                break;
            }

            if peek.is_ascii_whitespace() {
                if peek == '\r' {
                    self.next().unwrap();

                    if self.peek()? == '\n' {
                        self.next().unwrap();
                    }
                } else {
                    self.next().unwrap();
                }

                s.push(' ');
            } else if peek == '<' {
                return Err(XmlParseErrorKind::InvalidCharData('<'));
            } else if self.peek_str("&#")? {
                s.push(self.parse_char_ref()?);
            } else if peek == '&' {
                s.push_str(&self.parse_entity_reference()?);
            } else {
                s.push(self.next().unwrap());
            }
        }

        self.eat_char(quote_type)?;

        let s = {
            let mut new_s = String::with_capacity(s.len());

            let mut char_iter = s.trim_matches(|c| c == ' ').chars().peekable();

            while let Some(c) = char_iter.next() {
                if c == ' ' {
                    while let Some(&' ') = char_iter.peek() {
                        char_iter.next().unwrap();
                    }
                }

                new_s.push(c);
            }

            new_s
        };

        Ok(s)
    }

    fn parse_cdata(&mut self) -> Result<String, XmlParseErrorKind> {
        self.eat_str("<![CDATA[")?;

        let mut cdata = String::new();

        while !self.peek_str("]]>")? {
            let c = self.next()?;

            cdata.push(c);
        }

        self.eat_str("]]>")?;

        Ok(cdata)
    }
    fn parse_name(&mut self) -> Result<String, XmlParseErrorKind> {
        self.eat_whitespace()?;

        let peek = self.peek()?;

        if !is_letter(peek) && peek != '_' && peek != ':' {
            self.collect_while(|c| !c.is_ascii_whitespace())?;

            return Err(XmlParseErrorKind::InvalidTagName(self.buffer.clone()));
        }

        self.collect_while(is_name_char)?;
        Ok(self.buffer.clone())
    }

    fn parse_pubid_literal(&mut self) -> Result<String, XmlParseErrorKind> {
        let quote_type = match self.peek()? {
            c if c == '\'' || c == '"' => c,
            c => return Err(XmlParseErrorKind::UnexpectedChar(c)),
        };

        self.eat_char(quote_type)?;

        let mut s = String::new();

        while self.peek()? != quote_type {
            let next = self.next()?;
            if is_pubid_char(next) {
                s.push(next);
            } else {
                return Err(XmlParseErrorKind::InvalidPubIdLiteral(next));
            }
        }

        self.eat_char(quote_type)?;

        Ok(s)
    }

    fn parse_entity_value(&mut self) -> Result<String, XmlParseErrorKind> {
        let quote_type = match self.peek()? {
            c if c == '\'' || c == '"' => c,
            c => return Err(XmlParseErrorKind::UnexpectedChar(c)),
        };

        self.eat_char(quote_type).unwrap();

        let mut s = String::new();

        loop {
            match self.peek()? {
                c if c == quote_type => break,
                '%' => s.push_str(&self.parse_percent_reference()?),
                '&' => s.push_str(&self.parse_entity_reference()?),
                c => {
                    self.next().unwrap();
                    s.push(c);
                }
            }
        }

        self.eat_char(quote_type)?;

        Ok(s)
    }

    fn parse_system_literal(&mut self) -> Result<String, XmlParseErrorKind> {
        let quote_type = match self.peek()? {
            c if c == '\'' || c == '"' => c,
            c => return Err(XmlParseErrorKind::UnexpectedChar(c)),
        };

        self.eat_char(quote_type)?;
        self.collect_while(|c| c != quote_type)?;
        self.eat_char(quote_type)?;

        Ok(self.buffer.clone())
    }

    fn parse_processing_instruction(
        &mut self,
        position: Position,
    ) -> Result<XmlProcessingInstruction, XmlParseErrorKind> {
        self.eat_str("<?")?;

        let name = self.parse_name()?;

        if name.eq_ignore_ascii_case("xml") {
            return Err(XmlParseErrorKind::InvalidProcessingInstructionTarget);
        }

        self.eat_whitespace()?;

        let mut content = String::new();

        while !self.peek_str("?>")? {
            let c = self.next()?;

            if c == '\r' && self.peek()? == '\n' {
                self.next().unwrap();
                content.push('\n');
            } else {
                content.push(c);
            }
        }

        self.eat_str("?>")?;

        Ok(XmlProcessingInstruction {
            name,
            content,
            position,
        })
    }

    fn parse_entity_reference(&mut self) -> Result<String, XmlParseErrorKind> {
        self.eat_char('&')?;
        let name = self.parse_name()?;
        self.eat_char(';')?;

        Ok(match name.as_str() {
            "amp" => "&",
            "lt" => "<",
            "gt" => ">",
            "quot" => "\"",
            "apos" => "'",
            s => return Ok(format!("&{};", s)),
        }
        .into())
    }

    fn parse_percent_reference(&mut self) -> Result<String, XmlParseErrorKind> {
        self.eat_char('%')?;
        let name = self.parse_name()?;
        self.eat_char(';')?;

        Ok(format!("%{};", name))
    }

    fn parse_char_ref(&mut self) -> Result<char, XmlParseErrorKind> {
        self.eat_str("&#")?;

        let resolved = if self.peek()? == 'x' {
            self.eat_char('x').unwrap();
            self.collect_while(|c| {
                ('a'..='f').contains(&c) || ('0'..='9').contains(&c) || ('A'..='F').contains(&c)
            })?;

            u32::from_str_radix(&self.buffer, 16)
                .map_err(|_| XmlParseErrorKind::InvalidCharRef(self.buffer.clone()))?
        } else {
            self.collect_while(|c| ('0'..='9').contains(&c))?;

            u32::from_str_radix(&self.buffer, 10)
                .map_err(|_| XmlParseErrorKind::InvalidCharRef(self.buffer.clone()))?
        };

        self.eat_char(';')?;

        let c = std::char::from_u32(resolved)
            .ok_or_else(|| XmlParseErrorKind::InvalidCharRef(self.buffer.clone()))?;

        if !is_valid_char(c) {
            return Err(XmlParseErrorKind::InvalidCharRef(self.buffer.clone()));
        }

        Ok(c)
    }

    fn eat_eq(&mut self) -> Result<(), XmlParseErrorKind> {
        self.eat_whitespace()?;
        self.eat_char('=')?;
        self.eat_whitespace()?;

        Ok(())
    }

    fn eat_end_tag(&mut self, name: &str) -> Result<(), XmlParseErrorKind> {
        self.eat_str("</")?;
        let name2 = self.parse_name()?;

        if name != name2 {
            return Err(XmlParseErrorKind::UnexpectedClosingTagName(name2));
        }

        self.eat_whitespace()?;

        self.eat_char('>')?;

        Ok(())
    }

    fn eat_comment(&mut self) -> Result<(), XmlParseErrorKind> {
        self.eat_str("<!--")?;

        while !self.peek_str("--")? {
            self.next()?;
        }

        self.eat_str("-->")?;

        Ok(())
    }

    fn collect_while<F: Fn(char) -> bool>(&mut self, f: F) -> Result<(), XmlParseErrorKind> {
        self.buffer.clear();
        while f(self.peek()?) {
            let next = self.next()?;
            self.buffer.push(next);
        }
        Ok(())
    }
    fn skip_while<F: Fn(char) -> bool>(&mut self, f: F) -> Result<(), XmlParseErrorKind> {
        while f(self.peek()?) {
            self.next().unwrap();
        }

        Ok(())
    }

    fn eat_str(&mut self, s: &str) -> Result<(), XmlParseErrorKind> {
        for c in s.chars() {
            self.eat_char(c)?;
        }

        Ok(())
    }

    fn eat_char(&mut self, c: char) -> Result<(), XmlParseErrorKind> {
        let next = self.next()?;

        match next {
            ch if c == ch => Ok(()),
            ch => Err(XmlParseErrorKind::UnexpectedChar(ch)),
        }
    }

    fn eat_whitespace(&mut self) -> Result<(), XmlParseErrorKind> {
        loop {
            let c = self.peek()?;

            if ![' ', '\t', '\r', '\n'].contains(&c) {
                break;
            }

            self.next()?;
        }

        Ok(())
    }

    fn next(&mut self) -> Result<char, XmlParseErrorKind> {
        self.col += 1;

        let c = if !self.peek.is_empty() {
            self.peek.pop_front().unwrap()
        } else {
            self.read_char()?
        };

        if c == '\n' {
            self.line += 1;
            self.col = 1;
        }

        Ok(c)
    }

    fn peek(&mut self) -> Result<char, XmlParseErrorKind> {
        if let Some(peek) = self.peek.front() {
            Ok(*peek)
        } else {
            let c = self.read_char()?;
            self.peek.push_back(c);
            Ok(c)
        }
    }

    fn peek_n(&mut self, n: usize) -> Result<(), XmlParseErrorKind> {
        debug_assert!(n > 0);
        if self.peek.len() >= n {
            Ok(())
        } else {
            let already_peeked = self.peek.len();
            let to_peek = n - already_peeked;
            for _ in 0..to_peek {
                let c = match self.read_char() {
                    Ok(c) => c,
                    Err(XmlParseErrorKind::UnexpectedEndOfText) => {
                        return Ok(());
                    }
                    e => e?,
                };
                self.peek.push_back(c);
            }

            Ok(())
        }
    }

    fn peek_str(&mut self, s: &str) -> Result<bool, XmlParseErrorKind> {
        self.peek_n(s.len())?;

        Ok(self
            .peek
            .iter()
            .cloned()
            .zip(s.chars())
            .all(|(c1, c2)| c1 == c2))
    }

    fn read_char(&mut self) -> Result<char, XmlParseErrorKind> {
        let first_byte = self.read_byte()?;
        let byte_count = (!first_byte).leading_zeros() as usize;
        if byte_count == 0 {
            match first_byte {
                0x09 | 0x0A | 0x0D | 0x20..=0xFF => Ok(first_byte as char),
                _ => Err(XmlParseErrorKind::InvalidCharData(first_byte as char)),
            }
        } else {
            let mut bits = 0u32;
            let mask = 0xFF >> (byte_count + 1);
            let first_bits = u32::from(first_byte & mask);
            let shift = 6 * (byte_count - 1);
            bits |= first_bits << shift;
            for i in 1..byte_count {
                let byte = self.read_byte()?;
                // Following byte has an invalid bits 6 & 7
                if byte & 0b1100_0000 != 0b1000_0000 {
                    return Err(XmlParseErrorKind::InvalidUtf8);
                }
                let tmp = u32::from(byte & 0x3F);
                let shift_amount = 6 * (byte_count - i - 1);
                bits |= tmp << shift_amount;
            }

            let c = std::char::from_u32(bits).ok_or(XmlParseErrorKind::InvalidUtf8)?;

            if !is_valid_char(c) {
                Err(XmlParseErrorKind::InvalidCharData(c))
            } else {
                Ok(c)
            }
        }
    }

    fn read_byte(&mut self) -> Result<u8, XmlParseErrorKind> {
        let mut buf = [0];
        self.inner.read_exact(&mut buf)?;
        Ok(buf[0])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;
    use std::str::FromStr;

    #[test]
    fn simple() {
        let simple = r#"
<root>
    <foo bar="baz" qux='quack'>aaaa bbbb cccc</foo>
    henlo world    
    <!-- comment -->
</root>"#;

        let res = Parser::with_options(
            std::io::Cursor::new(simple),
            ParserOptions::new().whitespace_between_elements(true),
        )
        .parse();

        println!("{:?}", res);

        assert!(res.is_ok());
        let res = res.unwrap();
        assert_eq!(
            res,
            XmlDocument {
                xml_declaration: None,
                doctype_declarations: vec![],
                processing_instructions: vec![],
                root: XmlNode::Element(XmlElement {
                    attributes: Attributes::new(),
                    children: vec![
                        XmlNode::Text("\n    ".into()),
                        XmlNode::Element(XmlElement {
                            attributes: {
                                let mut attrs = Attributes::new();
                                attrs.insert("bar".into(), "baz".into());
                                attrs.insert("qux".into(), "quack".into());
                                attrs
                            },
                            children: vec![XmlNode::Text("aaaa bbbb cccc".into())],
                            name: "foo".into(),
                        }),
                        XmlNode::Text("\n    henlo world    \n    ".into()),
                        XmlNode::Text("\n".into()),
                    ],
                    name: "root".into(),
                })
            }
        );
    }

    #[test]
    fn char_ref() {
        let xml = "<root>&#x41;&#65;&#x130be;&#78060;</root>";
        let res = XmlDocument::from_str(xml).unwrap();

        assert_eq!(
            res.root,
            XmlNode::Element(XmlElement {
                name: "root".into(),
                attributes: Attributes::default(),
                children: vec![XmlNode::Text("AA𓂾𓃬".into()),]
            })
        );
    }

    #[test]
    fn xml_decl() {
        let decl = r#"
            <?xml version="foo" encoding="asdf" standalone="yes" ?>
            <root />
        "#;

        let res = XmlDocument::from_str(decl).unwrap();
        assert_eq!(
            res,
            XmlDocument {
                xml_declaration: Some(XmlDeclaration {
                    version: "foo".into(),
                    encoding: Some("asdf".into()),
                    standalone: Some(true)
                }),
                doctype_declarations: vec![],
                processing_instructions: vec![],
                root: XmlNode::Element(XmlElement {
                    attributes: Attributes::new(),
                    children: vec![],
                    name: "root".into(),
                }),
            }
        );
    }

    #[test]
    fn ignore_dtd() {
        let xml = "<!DOCTYPE doc [
<!ELEMENT doc (#PCDATA)>
]>
<doc ></doc>";

        let res = XmlDocument::from_str(xml).unwrap();
        assert_eq!(format!("{}", res.root), "<doc></doc>");

        let xml = r#"<!DOCTYPE doc [
<!ELEMENT doc (foo)>
<!ELEMENT foo (#PCDATA)>
<!ENTITY e "&#60;foo></foo>">
]>
<doc>&e;</doc>"#;
        let res = XmlDocument::from_str(xml).unwrap();
        assert_eq!(format!("{}", res.root), "<doc>&amp;e;</doc>");
    }

    #[test]
    fn normalize_attr_value() {
        let xml = "<root a1=\"   \t\t\t\t\t  foo      \t\t\t\t\t\tbar   \t\t\t\t\t          \" />";
        let res = XmlDocument::from_str(xml).unwrap();

        assert_eq!(res.root().attributes().get("a1").unwrap(), "foo bar");

        let xml = "<root a1=\"&#xd;&#xd;A&#xa;&#xa;B&#xd;&#xa;\" />";
        let res = XmlDocument::from_str(xml).unwrap();

        assert_eq!(res.root().attributes().get("a1").unwrap(), "\r\rA\n\nB\r\n");
    }

    #[test]
    fn trim_whitespace_between_elements() {
        let simple = r#"
<root>
    <foo bar="baz" qux='quack'>aaaa bbbb cccc</foo>
    henlo world    
    <!-- comment -->
</root>"#;

        let res = Parser::with_options(
            std::io::Cursor::new(simple),
            ParserOptions::new().whitespace_between_elements(false),
        )
        .parse();

        println!("{:?}", res);

        assert!(res.is_ok());
        let res = res.unwrap();
        assert_eq!(
            res,
            XmlDocument {
                xml_declaration: None,
                doctype_declarations: vec![],
                processing_instructions: vec![],
                root: XmlNode::Element(XmlElement {
                    attributes: Attributes::new(),
                    children: vec![
                        XmlNode::Element(XmlElement {
                            attributes: {
                                let mut attrs = Attributes::new();
                                attrs.insert("bar".into(), "baz".into());
                                attrs.insert("qux".into(), "quack".into());
                                attrs
                            },
                            children: vec![XmlNode::Text("aaaa bbbb cccc".into())],
                            name: "foo".into(),
                        }),
                        XmlNode::Text("\n    henlo world    \n    ".into()),
                    ],
                    name: "root".into(),
                })
            }
        );
    }

    #[test]
    fn issue_3_test() {
        let xml = "<note>
    <to>Tove</to>
    <from>Jani</from>
    <heading>Reminder</heading>
    <body>Don't forget me this weekend!</body>
</note>";

        assert!(XmlDocument::from_str(xml).is_ok());
    }

    #[test]
    fn trim_namespace_in_tags() {
        let xml = "<anamespace:foo />";
        let opts = ParserOptions::new().tag_name_namespaces(false);

        assert_eq!(
            XmlDocument::from_str_with_options(xml, opts)
                .unwrap()
                .root()
                .name(),
            "foo"
        );
    }
}
