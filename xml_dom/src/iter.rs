use crate::XmlNode;
use std::collections::VecDeque;

pub struct XmlIter<'a> {
    queue: VecDeque<&'a XmlNode>,
}

impl<'a> XmlIter<'a> {
    pub fn new(node: &'a XmlNode) -> Self {
        let mut queue = VecDeque::new();
        queue.push_front(node);

        Self { queue }
    }
}

impl<'a> Iterator for XmlIter<'a> {
    type Item = &'a XmlNode;

    fn next(&mut self) -> Option<Self::Item> {
        let next = self.queue.pop_front()?;

        if let XmlNode::Element(ele) = &next {
            for child in ele.children().iter().rev() {
                self.queue.push_front(child);
            }
        }

        Some(next)
    }
}

#[cfg(test)]
mod tests {
    use crate::{Attributes, XmlDocument, XmlElement, XmlNode};
    use pretty_assertions::assert_eq;
    use std::str::FromStr;

    #[test]
    fn test_iter() {
        let doc = XmlDocument::from_str("<root><child1>foo</child1><child2 /></root>").unwrap();
        let mut iter = doc.iter();

        assert_eq!(
            iter.next(),
            Some(&XmlNode::Element(XmlElement::new(
                "root",
                Attributes::default(),
                vec![
                    XmlNode::Element(XmlElement::new(
                        "child1",
                        Attributes::default(),
                        vec![XmlNode::Text(String::from("foo"))],
                    )),
                    XmlNode::Element(XmlElement::new("child2", Attributes::default(), vec![],)),
                ]
            )))
        );

        assert_eq!(
            iter.next(),
            Some(&XmlNode::Element(XmlElement::new(
                "child1",
                Attributes::default(),
                vec![XmlNode::Text(String::from("foo"))],
            )))
        );

        assert_eq!(iter.next(), Some(&XmlNode::Text(String::from("foo"))));

        assert_eq!(
            iter.next(),
            Some(&XmlNode::Element(XmlElement::new(
                "child2",
                Attributes::default(),
                vec![],
            )))
        );

        assert!(iter.next().is_none());
    }
}
