//! `xml_dom` provides an XML parser that produces an XML DOM with the eventual goal
//! of allowing for DTD validation upon user request.
//!
//! ## Example
//! ```rust
//! use std::str::FromStr;
//!
//! fn main() {
//!     let simple = r#"
//! <root>
//!     <foo bar="baz" qux='quack'>aaaa bbbb cccc</foo>
//!     <aaa><?processing instruction?></aaa>
//!     hello world
//!     <![CDATA[dis is some cdata text]]>
//!     <!-- comment -->
//! </root>"#;
//!
//!     let root = xml_dom::XmlDocument::from_str(simple).unwrap();
//!
//!     println!("{:#?}", root);
//! }
//! ```
//!
//! ## Derive Macros
//!
//! `xml_dom` has the ability to automatically serialize your structs (and soon™ enums) to XML elements (and by extension, text!).
//!
//! ### Example
//! ```rust
//! use xml_dom::ToXmlElement;
//!
//! #[derive(ToXmlElement)]
//! #[xml(rename = "my-struct")]
//! struct MyStruct {
//!     hey: i32,
//!     some: Option<u32>,
//!     fields: String,
//! }
//!
//! fn main() {
//!     let my_struct = MyStruct {
//!         hey: 1,
//!         some: Some(5),
//!         fields: "Yay!".into()
//!     };
//!
//!     println!("{}", my_struct.to_xml_element().pretty_print(2));
//! }
//! ```
//! This produces:
//! ```xml
//! <my-struct>
//!   <hey>1</hey>
//!   <some>5</some>
//!   <fields>Yay!</fields>
//! </my-struct>
//! ```
//!

#![deny(missing_docs)]
#![allow(dead_code)]

mod dtd;
mod iter;
mod parser;
mod pretty_print;

pub use parser::{ParserOptions, XmlParseError};
use std::{fmt, str::FromStr};
pub use xml_dom_macros::*;

macro_rules! xml_node_str_impl {
    ($($t:ty),+) => {
        $(impl ToXmlNode for $t {
            fn to_xml_node(&self) -> XmlNode {
                XmlNode::Text(self.to_string())
            }
        })+
    };
}

/// Type alias for `HashMap<String, String>`.
pub type Attributes = std::collections::HashMap<String, String>;

/// A trait to convert a type to an `XmlElement`.
///
/// Derive macro options:
///
///
/// `struct` options:
///
/// | Attribute                       | Purpose                                                           | Valid Values                                                                                                  |
/// |---------------------------------|-------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
/// | `#[xml(rename = "NAME")]`       | Renames the root struct                                           | Any valid XML element name                                                                                    |
/// | `#[xml(rename_all = "STYLE")]`  | Renames the root struct and all of the fields                     | `snake_case`, `kebab-case`, `camelCase`, `PascalCase`, `SCREAMING_SNAKE_CASE`, `UPPERCASE`, `lowercase`       |
/// | `#[xml(attr("NAME", "VALUE"))]` | Adds an attribute to the root element when generating XML element | Any valid XML attribute name & value                                                                          |
///
/// <br/>
///
/// `struct` field options:
///
/// | Attribute                  | Purpose                                                                                                                        | Valid Values                       |
/// |----------------------------|--------------------------------------------------------------------------------------------------------------------------------|------------------------------------|
/// | `#[xml(rename = "NAME")]`  | Renames the field                                                                                                              | Any valid XML element name         |
/// | `#[xml(attribute)]`        | Marks the field as being an attribute value and will be added to the attribute list on element generation                      | N/A                                |
/// | `#[xml(wrap_in = "NAME")]` | Convenience attribute to wrap the field in a parent element                                                                    | Any valid XML element name         |
/// | `#[xml(skip_if = "PATH")]` | Skip serialization of the field if the given function/method path returns `true`                                               | Path to valid Rust function/method |
/// | `#[xml(bare_value)]`       | Marks the field as being a bare value, it will not be wrapped in an element and instead the contents added directly to the XML | N/A                                |
/// | `#[xml(collection)]`       | Marks the field as being a collection (`Vec<_>`, `[_]`, etc), must have a `.iter()` method to be used                          | N/A                                |
///
pub trait ToXmlElement {
    /// Performs the conversion to an XML element.
    fn to_xml_element(&self) -> XmlElement;
}

impl ToXmlElement for XmlElement {
    fn to_xml_element(&self) -> XmlElement {
        self.clone()
    }
}

/// A trait to convert a type to an `XmlNode`.
pub trait ToXmlNode {
    /// Performs the conversion to an XML node.
    fn to_xml_node(&self) -> XmlNode;
}

impl<T: ToXmlElement> ToXmlNode for T {
    fn to_xml_node(&self) -> XmlNode {
        XmlNode::Element(self.to_xml_element())
    }
}

impl<T: ToXmlNode> ToXmlNode for Option<T> {
    fn to_xml_node(&self) -> XmlNode {
        match self {
            Some(inner) => inner.to_xml_node(),
            None => XmlNode::Text(String::new()),
        }
    }
}

xml_node_str_impl!(String, u8, u16, u32, u64, u128, i8, i16, i32, i64, i128, bool, str);

/// A trait to convert from an `XmlElement` to some other type.
pub trait FromXmlElement: Sized {
    /// Error type of the conversion.
    type Error;

    /// Performs the conversion from the `XmlElement`.
    fn from_xml_element(element: &XmlElement) -> Result<Self, Self::Error>;
}

/// The entire XML document
#[derive(Debug, Clone, PartialEq)]
pub struct XmlDocument {
    xml_declaration: Option<XmlDeclaration>,
    processing_instructions: Vec<XmlProcessingInstruction>,
    doctype_declarations: Vec<dtd::XmlDocTypeDeclaration>,
    root: XmlNode,
}

impl XmlDocument {
    /// Reference to the XML delcaration in the document, if one exists.
    pub fn xml_declaration(&self) -> Option<&XmlDeclaration> {
        self.xml_declaration.as_ref()
    }

    /// Slice of processing instructions that prefixed the root node.
    pub fn processing_instructions(&self) -> &[XmlProcessingInstruction] {
        &self.processing_instructions
    }

    /// The root element of the document.
    pub fn root(&self) -> &XmlElement {
        self.root.element().unwrap()
    }

    /// Consume the document, returning only the root element.
    pub fn into_root(self) -> XmlElement {
        match self.root {
            XmlNode::Element(element) => element,
            _ => unreachable!(),
        }
    }

    /// Creates a depth-first iterator over the XML nodes in the document.
    pub fn iter(&self) -> iter::XmlIter {
        iter::XmlIter::new(&self.root)
    }

    /// Parse a source string with the supplied parsing options.
    pub fn from_str_with_options(src: &str, options: ParserOptions) -> Result<Self, XmlParseError> {
        parser::Parser::with_options(std::io::Cursor::new(src), options).parse()
    }

    /// Attempt to parse an XML document from a readable source.
    pub fn from_reader<R: std::io::Read>(r: R) -> Result<Self, XmlParseError> {
        parser::Parser::new(r).parse()
    }

    /// Attempt to parse an XML document from a readable source with the supplied parsing options.
    pub fn from_reader_with_options<R: std::io::Read>(
        r: R,
        options: ParserOptions,
    ) -> Result<Self, XmlParseError> {
        parser::Parser::with_options(r, options).parse()
    }

    /// Get wrapper struct that pretty prints the document when displayed.
    pub fn pretty_print(&self, indent_length: u32) -> pretty_print::XmlPrettyPrinter<'_> {
        pretty_print::XmlPrettyPrinter {
            inner: pretty_print::DocOrElement::Doc(self),
            indent_length,
        }
    }
}

impl FromStr for XmlDocument {
    type Err = XmlParseError;

    fn from_str(s: &str) -> Result<Self, XmlParseError> {
        parser::Parser::new(std::io::Cursor::new(s)).parse()
    }
}

impl fmt::Display for XmlDocument {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(decl) = &self.xml_declaration {
            writeln!(f, "{}", decl)?;
        }

        for pi in self
            .processing_instructions()
            .iter()
            .filter(|pi| pi.position == parser::Position::Before)
        {
            write!(f, "{}", pi)?;
        }

        write!(f, "{}", self.root)?;

        for pi in self
            .processing_instructions()
            .iter()
            .filter(|pi| pi.position == parser::Position::After)
        {
            write!(f, "{}", pi)?;
        }

        Ok(())
    }
}

/// An XML version declaration.
///
/// ex. `<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>`
#[derive(Debug, Clone, PartialEq)]
pub struct XmlDeclaration {
    version: String,
    encoding: Option<String>,
    standalone: Option<bool>,
}

impl fmt::Display for XmlDeclaration {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<?xml version=\"{}\"", self.version)?;

        if let Some(encoding) = &self.encoding {
            write!(f, " encoding=\"{}\"", encoding)?;
        }

        if let Some(standalone) = self.standalone {
            write!(
                f,
                " standalone=\"{}\"",
                if standalone { "yes" } else { "no" }
            )?;
        }

        write!(f, " ?>")
    }
}

/// An XML node.
#[derive(Debug, Clone, PartialEq)]
pub enum XmlNode {
    /// An XML element.
    Element(XmlElement),
    /// Text.
    Text(String),
    /// Processing instruction.
    ProcessingInstruction(XmlProcessingInstruction),
}

impl XmlNode {
    /// Helper function to get a reference to the contained `XmlElement` if it
    /// exists, otherwise returns `None`.
    pub fn element(&self) -> Option<&XmlElement> {
        match self {
            XmlNode::Element(elem) => Some(elem),
            _ => None,
        }
    }

    /// Returns whether or not this node is an element
    pub fn is_element(&self) -> bool {
        match self {
            XmlNode::Element(_) => true,
            _ => false,
        }
    }

    /// Helper function to get a reference to the contained text if it exists,
    /// otherwise returns `None`.
    pub fn text(&self) -> Option<&str> {
        match self {
            XmlNode::Text(contents) => Some(contents.as_ref()),
            _ => None,
        }
    }

    /// Returns whether or not this node is text
    pub fn is_text(&self) -> bool {
        match self {
            XmlNode::Text(_) => true,
            _ => false,
        }
    }

    /// Helper function to get a reference to the contained
    /// `XmlProcessingInstruction` if it exists, otherwise returns `None`.
    pub fn processing_instruction(&self) -> Option<&XmlProcessingInstruction> {
        match self {
            XmlNode::ProcessingInstruction(pi) => Some(pi),
            _ => None,
        }
    }

    /// Returns whether or not this node is a processing instruction.
    pub fn is_processing_instruction(&self) -> bool {
        match self {
            XmlNode::ProcessingInstruction(_) => true,
            _ => false,
        }
    }
}

impl fmt::Display for XmlNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            XmlNode::Element(element) => write!(f, "{}", element),
            XmlNode::Text(contents) => escape_contents(&contents, f),
            XmlNode::ProcessingInstruction(pi) => write!(f, "{}", pi),
        }
    }
}

fn escape_contents(contents: &str, f: &mut fmt::Formatter) -> fmt::Result {
    for c in contents.chars() {
        match c {
            '<' => write!(f, "&lt;"),
            '>' => write!(f, "&gt;"),
            '&' => write!(f, "&amp;"),
            '\'' => write!(f, "&apos;"),
            '"' => write!(f, "&quot;"),
            '\n' => write!(f, "&#10;"),
            '\t' => write!(f, "&#9;"),
            '\r' => write!(f, "&#13;"),
            c => write!(f, "{}", c),
        }?;
    }

    Ok(())
}

/// An XML element, probably the most useful of the XML types. Can have
/// attributes and children.
///
/// ex. `<tag-name>content</tagname>`
#[derive(Debug, Clone, PartialEq, Default)]
pub struct XmlElement {
    attributes: Attributes,
    children: Vec<XmlNode>,
    name: String,
}

impl XmlElement {
    /// Create a new XML element.
    pub fn new<S: Into<String>>(
        name: S,
        attributes: Attributes,
        children: Vec<XmlNode>,
    ) -> XmlElement {
        XmlElement {
            attributes,
            children,
            name: name.into(),
        }
    }

    /// Slice of children nodes of the XML element.
    pub fn children(&self) -> &[XmlNode] {
        &self.children
    }

    /// The XML element's tag name.
    pub fn name(&self) -> &str {
        &self.name
    }

    /// `HashMap<String, String>` over any available attributes on the element.
    pub fn attributes(&self) -> &Attributes {
        &self.attributes
    }

    /// Like `element["name"]` but doesn't panic.
    pub fn get_child(&self, name: &str) -> Option<&XmlElement> {
        self.children
            .iter()
            .filter_map(|node| match node {
                XmlNode::Element(element) if element.name == name => Some(element),
                _ => None,
            })
            .nth(0)
    }

    /// Get wrapper struct that pretty prints the element when displayed.
    pub fn pretty_print(&self, indent_length: u32) -> pretty_print::XmlPrettyPrinter<'_> {
        pretty_print::XmlPrettyPrinter {
            inner: pretty_print::DocOrElement::Element(self),
            indent_length,
        }
    }
}

impl fmt::Display for XmlElement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<{}", self.name)?;

        for (name, value) in self.attributes() {
            write!(f, " {}=\"", name)?;
            escape_contents(&value, f)?;
            write!(f, "\"")?;
        }

        if self.children.is_empty() {
            //write!(f, " />")
            write!(f, "></{}>", self.name)
        } else {
            write!(f, ">")?;

            for child in self.children() {
                write!(f, "{}", child)?;
            }

            write!(f, "</{}>", self.name)
        }
    }
}

impl<'a> std::ops::Index<&'a str> for XmlElement {
    type Output = XmlElement;

    fn index(&self, idx: &'a str) -> &XmlElement {
        self.get_child(idx)
            .unwrap_or_else(|| panic!("No child element with the name \"{}\"", idx))
    }
}

impl<'a> std::ops::Index<usize> for XmlElement {
    type Output = XmlNode;

    fn index(&self, idx: usize) -> &XmlNode {
        &self.children[idx]
    }
}

/// Wrapper struct for indexing attributes by name.
pub struct Attr<'a>(pub &'a str);

impl<'a> std::ops::Index<Attr<'a>> for XmlElement {
    type Output = str;

    fn index(&self, idx: Attr) -> &str {
        self.attributes[idx.0].as_str()
    }
}

/// An XML processing instruction.
///
/// ex. `<?processing-instruction foo="bar" ?>`
#[derive(Debug, Clone, PartialEq)]
pub struct XmlProcessingInstruction {
    name: String,
    content: String,
    position: parser::Position,
}

impl fmt::Display for XmlProcessingInstruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<?{} {}?>", self.name, self.content)
    }
}
