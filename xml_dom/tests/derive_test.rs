use pretty_assertions::assert_eq;
use xml_dom::*;

#[test]
fn simple_serialize() {
    #[derive(ToXmlElement)]
    #[xml(rename = "foo")]
    struct Foo {
        bar: String,
    }

    assert_eq!(
        &Foo { bar: "baz".into() }.to_xml_element().to_string(),
        "<foo><bar>baz</bar></foo>",
    );
}

#[test]
fn simple_optional_serialize() {
    #[derive(ToXmlElement)]
    #[xml(rename = "foo")]
    struct Foo {
        #[xml(skip_if = "Option::is_none")]
        bar: Option<String>,
    }

    assert_eq!(
        &Foo {
            bar: Some("baz".into()),
        }
        .to_xml_element()
        .to_string(),
        "<foo><bar>baz</bar></foo>",
    );

    assert_eq!(
        &Foo { bar: None }.to_xml_element().to_string(),
        "<foo></foo>",
    );
}

#[test]
fn simple_optional_serialize_empty() {
    #[derive(ToXmlElement)]
    #[xml(rename = "foo")]
    struct Foo {
        bar: Option<String>,
    }

    assert_eq!(
        &Foo {
            bar: Some("baz".into()),
        }
        .to_xml_element()
        .to_string(),
        "<foo><bar>baz</bar></foo>",
    );

    assert_eq!(
        &Foo { bar: None }.to_xml_element().to_string(),
        "<foo><bar></bar></foo>",
    );
}

#[test]
fn collection_test() {
    #[derive(ToXmlElement)]
    #[xml(rename_all = "snake_case")]
    struct Qux {
        elem: String,
    }

    #[derive(ToXmlElement)]
    #[xml(rename_all = "snake_case")]
    struct Baz {
        #[xml(collection)]
        quxes: Vec<Qux>,
    }

    assert_eq!(
        &Baz {
            quxes: vec![Qux { elem: "A".into() }, Qux { elem: "B".into() }]
        }
        .to_xml_element()
        .to_string(),
        "<baz><quxes><qux><elem>A</elem></qux><qux><elem>B</elem></qux></quxes></baz>"
    );

    #[derive(ToXmlElement)]
    #[xml(rename_all = "snake_case")]
    struct Baz2 {
        #[xml(bare_value, collection)]
        quxes: Vec<Qux>,
    }

    assert_eq!(
        &Baz2 {
            quxes: vec![Qux { elem: "A".into() }, Qux { elem: "B".into() }]
        }
        .to_xml_element()
        .to_string(),
        "<baz2><qux><elem>A</elem></qux><qux><elem>B</elem></qux></baz2>"
    );
}

#[test]
fn ns_test() {
    #[derive(ToXmlElement)]
    #[xml(rename_all = "snake_case")]
    #[xml(ns = "mynamespace")]
    struct Qux {}

    assert_eq!(Qux {}.to_xml_element().name(), "mynamespace:qux");

    #[derive(ToXmlElement)]
    #[xml(rename_all = "snake_case")]
    #[xml(ns = "mynamespace")]
    struct Bar {
        #[xml(ns = "othernamespace")]
        field: bool,
    }

    assert_eq!(
        Bar { field: true }.to_xml_element()[0]
            .element()
            .unwrap()
            .name(),
        "othernamespace:field"
    );
}

macro_rules! test_case {
    ($(($tname:ident, $sname:ident, $case:literal, $wanted:literal)),+) => {
        $(
            #[test]
            fn $tname() {
                use xml_dom::ToXmlElement;

                #[allow(non_camel_case_types)]
                #[derive(ToXmlElement)]
                #[xml(rename_all = $case)]
                struct $sname {}

                pretty_assertions::assert_eq!($sname {}.to_xml_element().name(), $wanted);
            }
        )+
    }
}

test_case! {
    (camel_case, TestStruct, "camelCase", "testStruct"),
    (pascal_case, test_struct, "PascalCase", "TestStruct"),
    (snake_case, TestStruct, "snake_case", "test_struct"),
    (kebab_case, TestStruct, "kebab-case", "test-struct"),
    (screaming_snake_case, TestStruct, "SCREAMING_SNAKE_CASE", "TEST_STRUCT"),
    (uppercase, TestStruct, "UPPERCASE", "TESTSTRUCT"),
    (lowercase, TestStruct, "lowercase", "teststruct")
}
