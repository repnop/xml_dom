use std::{fs, str::FromStr};
use xml_dom::{XmlDocument, XmlParseError};

type TestErr = (Option<XmlParseError>, Option<(String, String)>, String);

#[test]
#[ignore]
fn valid_standalone_files() {
    let files = fs::read_dir("./tests/W3C XML Test Documents/valid/sa")
        .unwrap()
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    let results: Vec<_> = files
        .iter()
        .filter(|r| {
            r.file_type().unwrap().is_file()
                && r.file_name().into_string().unwrap().contains(".xml")
        })
        .map(|file| -> Result<(), TestErr> {
            let file_name = file.file_name().into_string().unwrap();
            let file_contents = match fs::read_to_string(file.path()) {
                Ok(contents) => contents,
                Err(ref e) if e.kind() == std::io::ErrorKind::InvalidData => {
                    // Some documents aren't UTF-8, so skip them for now
                    eprintln!("File not UTF-8: {}", file_name);
                    return Ok(());
                }
                e => e.unwrap(),
            };
            let document = XmlDocument::from_str(&file_contents)
                .map_err(|e| (Some(e), None, file_name.clone()))?;
            let check_file = format!("./tests/W3C XML Test Documents/valid/sa/out/{}", file_name);
            let should_be = fs::read_to_string(&check_file).unwrap();
            let outp = format!("{}", document);
            if should_be != outp {
                Err((
                    None,
                    Some((should_be, outp)),
                    file.file_name().into_string().unwrap(),
                ))
            } else {
                Ok(())
            }
        })
        .filter_map(|r| r.err())
        .collect();

    if !results.is_empty() {
        panic!("Following files failed ({}): {:#?}", results.len(), results);
    }
}
