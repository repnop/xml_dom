use std::{fs, str::FromStr};
use xml_dom::XmlDocument;

#[test]
#[ignore]
fn not_wellformed_standalone_files() {
    let mut files = fs::read_dir("./tests/W3C XML Test Documents/not-wf/sa")
        .unwrap()
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    files.sort_by(|e1, e2| e1.file_name().cmp(&e2.file_name()));

    let mut results: Vec<_> = files
        .iter()
        .filter(|r| {
            r.file_type().unwrap().is_file()
                && r.file_name().into_string().unwrap().contains(".xml")
        })
        .map(|file| -> Result<(), String> {
            let file_name = file.file_name().into_string().unwrap();
            let file_contents = match fs::read_to_string(file.path()) {
                Ok(contents) => contents,
                Err(ref e) if e.kind() == std::io::ErrorKind::InvalidData => {
                    // Some documents aren't UTF-8, so skip them for now
                    eprintln!("File not UTF-8: {}", file_name);
                    return Ok(());
                }
                e => e.unwrap(),
            };
            let document = XmlDocument::from_str(&file_contents);

            match document {
                Ok(_) => Err(file_name),
                Err(_) => Ok(()),
            }
        })
        .filter_map(|r| r.err())
        .collect();

    results.sort_by(|e1, e2| e1.cmp(&e2));

    if !results.is_empty() {
        panic!(
            "Following files parsed when they shouldn't have ({}): {:#?}",
            results.len(),
            results
        );
    }
}
