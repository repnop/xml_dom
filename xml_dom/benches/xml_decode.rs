#[macro_use]
extern crate criterion;

use criterion::{black_box, Criterion};
use std::str::FromStr;
use xml_dom::XmlDocument;

fn criterion_benchmark(c: &mut Criterion) {
    let files = std::fs::read_dir("./benches/xml_files/").unwrap();

    for file in files {
        let file = file.unwrap();
        c.bench_function(&file.file_name().into_string().unwrap(), move |b| {
            let contents = std::fs::read_to_string(file.path()).unwrap();
            b.iter(move || black_box(XmlDocument::from_str(&contents).unwrap()))
        });
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
